let socket_io = require('socket.io');
const cfg = require('./config')
let io = socket_io();
let socketAPI = {};

// MODBUS RTU
var ModbusRTU = require("modbus-serial");
var client = new ModbusRTU();

var UInt16 = function (value) {
    return (value & 0xffff);
}
var UInt32 = function (value) {
    return (value & 0xffffffff);
}

async function reConnect() {
    try{
        console.log('reconnecting ...')
        client.connectTCP(cfg.RTU.IP, { port: cfg.RTU.PORT });
        client.setID(cfg.RTU.ClientID)
    }catch (error) {
        console.error(error);
    }

}


async function readSingle(address,coeff,id,event){
    // console.log('connected!')
    io.emit('connectionStatus', true)
    // Send command to read data 
    // console.log(address)
    client.readHoldingRegisters(address, 1, function (err, data) {
        if(!err)
            io.emit(event, {value: data.data[0] / coeff, id: id})
        else{
            console.log(err)
        }
    });
}

async function readDouble( address,coeff,id, event){
    // Send command to read data 
    client.readHoldingRegisters(address, 2, function (err2, data2) {
        if (err2) console.log(err2)
        else {
            data = data2.buffer.readInt32BE(0) / coeff
            io.emit(event, {value: data, id: id})
        }
    })
}

async function write(address, coeff, value){
    client.writeRegisters(address, [UInt16(value * coeff)], (err) => {
        if (err) console.log(err)
    })
}

//Your socket logic here
socketAPI.io = io;
io.on('connection', socket => {
    socket.on('writeActivePowerRaw', (data)=>{
        let {id, value}= data
        write(cfg.unit[id].P_Addr, cfg.unit[id].P_Coeff, value)
    })
    
    socket.on('writeActivePowerPercentage', (data)=>{
        console.log(data)
        let {id, value}= data
        write(cfg.unit[id].PP_Addr, cfg.unit[id].PP_Coeff, value)
    })
})

// MODBUS RTU CODE LOGIC
setInterval(async function () {
    console.log('interval')
    if(client.isOpen){
        
        for(let i = 0; i < cfg.unit.length; i++){
            let u = cfg.unit[i]
            await readSingle(u.P_Addr, u.P_Coeff, i, 'readActivePowerRaw')
            await readSingle(u.PP_Addr, u.PP_Coeff, i, 'readActivePowerPercentage')
            // await readDouble(u.Q_Addr, u.Q_Coeff, i, 'readReactivePower')
        }
    }
    else{
        reConnect()
    }
    
    

    // Test:
    // io.emit('receivedDataPercentage', Math.round(Math.random() * 100))
    // io.emit('receivedDataReactive', Math.round(Math.random() * 1000))
    // io.emit('receivedDataRaw', Math.round(Math.random() * 10 ))

}, cfg.CONSTANT.UPDATE_INTERVAL * 1000);



module.exports = {
    socketAPI,
};
