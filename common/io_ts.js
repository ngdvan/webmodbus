let socket_io = require('socket.io');
let io = socket_io();
let socketAPI = {};

// MODBUS RTU
var ModbusRTU = require("modbus-serial");
var client = new ModbusRTU();
const IP = "172.16.76.110"
const PORT = 502
const DEFAULT_ID = 1
let isConnect = false

const REGISTER_POWER_PERCENTAGE = 30125
const REGISTER_POWER_RAW = 30120
const REGISTER_REACTIVE = 30129


var UInt16 = function (value) {
    return (value & 0xffff);
}
var UInt32 = function (value) {
    return (value & 0xffffffff);
}
function reConnect(){
    try{
        client.connectTCP(IP, { port: PORT });
        client.setID(DEFAULT_ID);
    }catch (error) {
        console.error(error);
    }
}

//Your socket logic here
socketAPI.io = io;
io.on('connection', socket => {
    socket.on('updateData', ()=>{
        console.log('update data')
        
        if(!client.isOpen){
            reConnect()
        }
        else{
            // Send command to read data 
            client.readHoldingRegisters(REGISTER_POWER_PERCENTAGE, 1, function(err, data) {
		console.log(data.data[0])
                io.to(socket.id).emit('receivedData', data.data[0])
            });
        }
    })

    socket.on('powerRaw', (powerRaw)=> {
        console.log(powerRaw)
        // Write command for raw power in kW
        if(!client.isOpen){
            reConnect()
        }
        else{
            client.writeRegisters(REGISTER_POWER_RAW, [UInt16(powerRaw)], (err)=>{
                if(err) console.log(err)
            })
        }
    })

    socket.on('powerPercentage', (powerPercentage) => {
        console.log(powerPercentage)
        // Write command for percentage Power in %
        if(!client.isOpen){
            reConnect()
        }
        else{
            client.writeRegisters(REGISTER_POWER_PERCENTAGE, [[UInt16(powerPercentage)]],(err)=>{
                if(err) console.log(err)
            })
        }
    })

    socket.on('reactive', (reactive) => {
        console.log(reactive)
        // Write command for percentage Power in %
        if(!client.isOpen){
            reConnect()
        }
        else{
            client.writeRegisters(REGISTER_REACTIVE, [[reactive]],(err)=>{
                if(err) console.log(err)
            })
        }
    })
})

// MODBUS RTU CODE LOGIC

// Every 1 second, trying to read data from MODBUS
// IF MODBUS is not connected, attempt to connect
setInterval(function() {
    if(client.isOpen){
        for (let i = 0; i < 3; i++){
            client.readHoldingRegisters(REGISTER_POWER_PERCENTAGE - i*10000, 1, function(err, data) {
                if(!err){
                    console.log(REGISTER_POWER_PERCENTAGE - i*10000, data.data[0]);
                }
            });    
        }
        
    }
    else{
        // RECONNECT ?
        isConnect = false
        reConnect()
    }
    
}, 2 * 1000);





module.exports = {
    socketAPI,
};
