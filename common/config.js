module.exports = {
    RTU : {
        IP: "172.16.76.110",
        PORT: 502,
        ClientID: 1
    },
    unit: [
        // Default: First item => TOTAL SETPOINT
        {
            P_Addr: 30120,
            P_Coeff: 10,
            Q_Addr: 30129,
            Q_Coeff: 1000,
            PP_Addr: 30125,
            PP_Coeff:10,
            name: "TOTAL SETPOINT",
        },
        // FROM 2nd item: Inverter. Change corressponding addresses
        {
            P_Addr: 10120,
            P_Coeff: 10,
            Q_Addr: 10129,
            Q_Coeff: 1000,
            PP_Addr: 10125,
            PP_Coeff:10,
            name: "INVERTER1"
        },
        {
            P_Addr: 20120,
            P_Coeff: 10,
            Q_Addr: 20129,
            Q_Coeff: 1000,
            PP_Addr: 20125,
            PP_Coeff:10,
            name: "INVERTER2"
        }
    ],
    CONSTANT : {
        UPDATE_INTERVAL: 5,  // time in seconds to update the datapoint
    }

}