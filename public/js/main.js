
const socket = io()

socket.on('readActivePowerRaw', data => {
    $(`#raw-value-${data.id}`).text(data.value)
})

socket.on('readActivePowerPercentage', data => {
    $(`#percentage-value-${data.id}`).text(data.value)
})

socket.on('readReactivePower', data => {
    $(`#reactive-value-${data.id}`).text(data.value)
})

socket.on('connectionStatus', data =>{
    $('#connection-status').prop('checked', data)
    console.log('connection ', data)
})

$("[id^=raw-input-]").on('change', function() {
    
    let id = ($(this).attr('id').split('-'))[2]
    let val = $(this).val()
    let isEnableAndActive = $(`#enable-${id}`).prop('checked') && $(`#raw-active-${id}`).prop('checked')
    if(isEnableAndActive){
        
        console.log('sending: ', val)
        socket.emit('writeActivePowerRaw', {id: id, value: val})    
    }
    else{
        console.log('not sending: ', val)
    }
})

$("[id^=percentage-input-]").on('change', function() {
    let id = ($(this).attr('id').split('-'))[2]
    let val = $(this).val()
    let isEnableAndActive = $(`#enable-${id}`).prop('checked') && $(`#percentage-active-${id}`).prop('checked')
    if(isEnableAndActive){
        
        console.log('sending: ', val)
        socket.emit('writeActivePowerPercentage', {id: id, value: val})
    }
    else{
        console.log('not sending: ', val)
    }
})

$("[id^=enable-]").on('change', function() {
    let id = ($(this).attr('id').split('-'))[1]
    let isEnable = $(this).prop('checked')
    console.log('id: ', id, ' isEnable: ', isEnable)
    // case total
    if(id == 0 && isEnable){
        for(let i = 1; i < unit.length; i++){
            $(`#enable-${i}`).prop('checked', false)
        }
    }
    // case inverter
    if(id > 0 && isEnable){
        $(`#enable-0`).prop('checked', false)
    }
    
})