var express = require('express');
var router = express.Router();
const cfg = require('../common/config')
/* GET home page. */
router.get('/', function(req, res, next) {
  let unit = cfg.unit.map(
    (currentValue, index, arr)=>{
      return {id: index, name: currentValue.name}
    }
  )
  res.render('index', { unit });
});

module.exports = router;
